import React, {Component} from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios'
import Button from 'react-bootstrap/lib/Button'

//This Component is a child Component of Customers Component
export default class CustomerDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.getCustomerDetails(this.props.val)
  }

  //Function which is called whenver the component is updated
  componentDidUpdate(prevProps) {

    //get Customer Details only if props has changed
    if (this.props.val !== prevProps.val) {
        if(this.props.val === undefined)
            this.props.val = 1;
      this.getCustomerDetails(this.props.val)
    }
  }

  //Function to Load the customerdetails data from json.
  getCustomerDetails(id) {
    console.log(id);
    if(id === undefined)
        id = 1;
    axios.get('assets/samplejson/customer' + id + '.json').then(response => {
      console.log("response: "+response)
      this.setState({customerDetails: response})
    })
  };

  render() {
    if (!this.state.customerDetails)
      return (<p>Loading Data</p>)
    return (<div className="customerdetails">
      <Panel bsStyle="info" className="centeralign">
        <Panel.Heading>
          <Panel.Title componentClass="h3">{this.state.customerDetails.data.title}</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
         <p>Description :</p> <input>{this.state.customerDetails.data.description}</input>
          <p>Status : </p> {this.state.customerDetails.data.status}
          <p>Create Date : {this.state.customerDetails.data.createdAt}</p>
        </Panel.Body>
        <Button bsStyle="info" onClick={() => this.setState()}>Edit</Button>
        <Button bsStyle="info" onClick={() => this.setState()}>Delete</Button>
      </Panel>
    </div>)
  }
}
