import React, {Component} from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import Button from 'react-bootstrap/lib/Button'
import CustomerDetails from './CustomerDetails'
import axios from 'axios'
//import MaterialTable from 'material-table';

export default class Customers extends Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedCustomer: 1
    }
  }
  

  //function which is called the first time the component loads
  componentDidMount() {
    this.getCustomerData();
  }

  //Function to get the Customer Data from json
  getCustomerData() {
    axios.get('assets/samplejson/dataTodoList.json').then(response => {
      console.log(response)
      this.setState({customerList: response})
    })
  };

  render() {
    if (!this.state.customerList)
      return (<p>Loading data</p>)
      return (<div className="addmargin">
      <div className="col-md-3">
        {
          this.state.customerList.data.map(customer => <Panel bsStyle="info" key={customer.title} className="centeralign">
       
            <Panel.Heading>
              <Panel.Title componentClass="h3">{customer.title}</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              <p>{customer.description}</p>
              <p>{customer.status}</p>
              <p>{customer.createdAt}</p>
              <Button bsStyle="info" onClick={() => this.setState({selectedCustomer: customer.id})}>

                Click to View Details

              </Button>

            </Panel.Body>
          </Panel>)
        }
      </div>
       <div className="col-md-6">
        <CustomerDetails val={this.state.selectedCustomer}/>
      </div> 
    </div>)

    
  }

}
