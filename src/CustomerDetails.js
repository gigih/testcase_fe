import React, {Component} from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios'
import Button from 'react-bootstrap/lib/Button'
//import CustomersUpdate from './CustomersUpdate'

//This Component is a child Component of Customers Component
export default class CustomerDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {selectedCustomer : 1}
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.getCustomerDetails(this.props.val)
  }

  //Function which is called whenver the component is updated
  componentDidUpdate(prevProps) {

    //get Customer Details only if props has changed
    if (this.props.val !== prevProps.val) {
      this.getCustomerDetails(this.props.val)
    }
  }

  //Function to Load the customerdetails data from json.
  getCustomerDetails(id) {
    console.log(id);
    axios.get('assets/samplejson/customer' + id + '.json').then(response => {
      console.log("response: "+response)
      this.setState({customerDetails: response})
    })
  };

  render() {
    if (!this.state.customerDetails)
      return (<p>Loading Data</p>)
    return (<div className="customerdetails">
      <Panel bsStyle="info" className="centeralign">
        <Panel.Heading>
          <Panel.Title componentClass="h3">{this.state.customerDetails.data.title}</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <p>Description : {this.state.customerDetails.data.description}</p>
          <p>Status : {this.state.customerDetails.data.status}</p>
          <p>Create Date : {this.state.customerDetails.data.createdAt}</p>
        </Panel.Body>
        <Button bsStyle="info" onClick={() => this.setState()}>Edit</Button>
        <Button bsStyle="info" onClick={() => this.setState()}>Delete</Button>
      </Panel>
      
      {/* <div className="col-md-6">
        <CustomersUpdate val={this.state.selectedCustomer}/>
      </div>  */}
    </div>)
  }
}
